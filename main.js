$( document ).ready( function() {
    // Set variables for the control elements, all selected by their IDs
    var rotation                 = $('#rot_range'),
        rotationDisplay          = $("#rot_disp"),
        rotationCenter           = $("#rot_cent"),
        speed                    = $('#speed_range'),
        speedDisplay             = $("#speed_disp"),
        speedCenter              = $("#speed_cent"),
        emergencyButton          = $("#emgstop"),
        collectorPowerSwitch     = $("#coll_pwr_switch"),
        collectorDirectionSwitch = $("#coll_dir_switch"),
        // Cannot use oninput with jQuery
        rotationDOM              = document.getElementById("rot_range"),
        speedDOM                 = document.getElementById("speed_range"),
        // The url to post data to
        postUrl                  = "/post"
        ;

    // How to sed the data to the server
    postData = function() {
        url = postUrl;
        // send wether the collector is on, its direction, the speed of the motors and the direction to turn
        data = {
            power: collectorPowerSwitch.is(':checked'),
            pickUp: collectorDirectionSwitch.is(':checked'),
            rotation: rotation.val(),
            speed: speed.val(),
        };
        $.post( url , data , function(data, status){
            //alert("Data: " + data + "\nStatus: " + status);
        });
    };

    updateUrl = function() {
        // When updating the post URL, if the input box is not empty, 
        // set the URL to // + the input + /post. 
        // If the box is empty, we want to post to a relative path, 
        // so do not put // in front.
        input = $("#serverIPinput").val();
        if( input == "" ){
            postUrl = $("#serverIPinput").val() + '/post'
        } else {
            postUrl = '//' + $("#serverIPinput").val() + '/post';
        }
        $("#serverIP").text( postUrl );
    };

    // Display default values when the page loads. The values is probably 0, unless the reload occured because of a refresh
    rotationDisplay.val( rotation.val() );
    speedDisplay.val( speed.val() );
    // update the post URL
    updateUrl();

    // Stop everything when the emergency button is clicked by setting all values to 0 and posting
    emergencyButton.click( function(){
        rotation.val( 0 );
        rotationDisplay.val( 0 );
        speed.val( 0 );
        speedDisplay.val( 0 );
        collectorDirectionSwitch.prop( 'checked', false );
        collectorPowerSwitch.prop( 'checked', false );
        postData();
    });

    // Change the value display when sliders change
    rotationDOM.oninput = function() {
        rotationDisplay.val( rotation.val() );
        postData()
    }
    speedDOM.oninput = function() {
        speedDisplay.val( speed.val() );
        postData()
    }

    // Reset sliders when buttons pressed
    rotationCenter.click( function() {
        rotation.val( 0 );
        rotationDisplay.val( 0 );
    });
    speedCenter.click( function() {
        speed.val( 0 );
        speedDisplay.val( 0 );
    });

    // Send post when inputs have been changed
    rotation.change( function() {
        postData()
    });
    speed.change( function() {
        postData()
    });
    rotationCenter.click( function() {
        postData()
    });
    speedCenter.click( function() {
        postData()
    });
    collectorPowerSwitch.change( function() {
        postData()
    });
    collectorDirectionSwitch.change( function() {
        postData()
    });

    // Making it possible to change the IP to post to
    $("#serverIPchange").click( function() {
        updateUrl();
    });
});